DROP DATABASE IF EXISTS Beijing_2022_Winter_Olympics;
CREATE DATABASE Beijing_2022_Winter_Olympics; 
USE Beijing_2022_Winter_Olympics;

-- indexes
DROP TABLE IF EXISTS Events; show warnings;
DROP TABLE IF EXISTS AchievedResults; show warnings;
DROP TABLE IF EXISTS Athletes; show warnings;
DROP TABLE IF EXISTS Countries; show warnings;
DROP TABLE IF EXISTS OlympicCommitteeMembers; show warnings;
DROP TABLE IF EXISTS Roles; show warnings;

-- Create the relational tables

-- 1. Events
CREATE TABLE Events (
    ID INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(80) NOT NULL,
    discipline VARCHAR(100),
    time TIME,
    date DATE,
    location VARCHAR(150) DEFAULT 'TBD',
    PRIMARY KEY (ID)
);

-- 2. Countries
CREATE TABLE Countries (
    code VARCHAR(3) NOT NULL UNIQUE,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (code)
);

-- 3. Athletes
CREATE TABLE Athletes (
  ID INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  DOB DATE,
  gender VARCHAR(6) NOT NULL,
  country_code VARCHAR(3) NOT NULL,
  residence_country_code VARCHAR(3),
  birth_country_code VARCHAR(3),
  height_in_cm NUMERIC(18,3),
  PRIMARY KEY (ID),
  FOREIGN KEY (country_code) references Countries(code)
);

-- 4. OlympicCommitteeMembers
CREATE TABLE OlympicCommitteeMembers (
  ID INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  gender VARCHAR(6),
  country_code VARCHAR(4) NOT NULL,
  PRIMARY KEY (ID),
  FOREIGN KEY (country_code) references Countries(code)
);

-- 5. Roles
CREATE TABLE Roles (
  member_ID INT NOT NULL, 
  role_name VARCHAR(100) NOT NULL,
  PRIMARY KEY (member_ID, role_name),
  FOREIGN KEY (member_ID) references OlympicCommitteeMembers(ID),
  CHECK (member_ID > 0)
);

-- 6. AchievedResults 
CREATE TABLE AchievedResults (
  event_ID INT NOT NULL,
  athlete_ID INT NOT NULL,
  result INT NOT NULL,
  PRIMARY KEY (event_ID, athlete_ID),
  FOREIGN KEY (event_ID) references Events(ID),
  FOREIGN KEY (athlete_ID) references Athletes(ID)
);

-- ALTER COMMANDS

-- Athletes
SHOW CREATE TABLE Athletes;

ALTER TABLE Athletes DROP FOREIGN KEY `athletes_ibfk_1`;
ALTER TABLE Athletes DROP FOREIGN KEY `athletes_ibfk_2`;
ALTER TABLE Athletes DROP FOREIGN KEY `athletes_ibfk_3`;

ALTER TABLE Athletes
ADD FOREIGN KEY (country_code) REFERENCES Countries(code) ON DELETE CASCADE ON UPDATE CASCADE,
ADD FOREIGN KEY (residence_country_code) REFERENCES Countries(code) ON DELETE CASCADE ON UPDATE CASCADE,
ADD FOREIGN KEY (birth_country_code) REFERENCES Countries(code) ON DELETE CASCADE ON UPDATE CASCADE;

-- AchievedResults
SHOW CREATE TABLE AchievedResults;
ALTER TABLE AchievedResults DROP FOREIGN KEY `achievedresults_ibfk_1`;
ALTER TABLE AchievedResults DROP FOREIGN KEY `achievedresults_ibfk_2`;
-- ALTER TABLE AchievedResults DROP CONSTRAINT `achievedresults_chk_4`;
-- ALTER TABLE AchievedResults DROP CONSTRAINT `achievedresults_chk_5`;
-- ALTER TABLE AchievedResults DROP CONSTRAINT `achievedresults_chk_6`;

ALTER TABLE AchievedResults
ADD FOREIGN KEY (event_ID) REFERENCES Events(ID) ON DELETE CASCADE,
ADD FOREIGN KEY (athlete_ID) REFERENCES Athletes(ID) ON DELETE CASCADE;

ALTER TABLE AchievedResults
ADD CHECK (result > 0),
ADD CHECK (event_ID > 0),
ADD CHECK (athlete_ID > 0);

-- Roles
SHOW CREATE TABLE Roles;

ALTER TABLE Roles DROP FOREIGN KEY `roles_ibfk_1`;

ALTER TABLE Roles
ADD FOREIGN KEY (member_ID) references OlympicCommitteeMembers(ID) ON DELETE CASCADE;

ALTER TABLE Roles ALTER role_name SET DEFAULT 'Coordinator';

-- OlympicCommitteeMembers
SHOW CREATE TABLE OlympicCommitteeMembers;

ALTER TABLE OlympicCommitteeMembers DROP FOREIGN KEY `olympiccommitteemembers_ibfk_1`;

ALTER TABLE OlympicCommitteeMembers
ADD FOREIGN KEY (country_code) references Countries(code) ON DELETE CASCADE ON UPDATE CASCADE;

-- Insert the data into the tables, used this data source - https://www.kaggle.com/datasets/piterfm/beijing-2022-olympics
-- Application is based on the Beijing 2022 Winter Olympics Data
-- I used the data from the csv files given, although to automate it, I would have to clean up the csv's and then load it automatically
-- Right now just using INSERT statements to populate the tables

-- 1. Insert data into the Events table
INSERT INTO Events(name, discipline, time, date, location) VALUES ('Men''s Downhill 1st Training', 'Alpine Skiing',
					'11:00:00', '2022-02-03', 'Yanqing National Alpine Skiing Centre - Speed'); -- 1
INSERT INTO Events(name, discipline, time, date, location) VALUES ('Men''s Downhill 2nd Training', 'Alpine Skiing',
					'12:00:00', '2022-02-04', 'Yanqing National Alpine Skiing Centre - Speed'); -- 2
INSERT INTO Events(name, discipline, time, date) VALUES ('Women''s Giant Slalom Run 1', 'Alpine Skiing', '09:30:00', '2022-02-07'); -- 3
INSERT INTO Events(name, discipline, time, date) VALUES ('Mixed Relay 4x6km (W+M)', 'Biathlon', '17:00:00', '2022-02-05'); -- 4
INSERT INTO Events(name, discipline, time, date) VALUES ('2-man Official Training Heat 3', 'Bobsleigh', '14:10:00', '2022-02-11'); -- 5
INSERT INTO Events(name, discipline, time, date) VALUES ('Women''s 7.5km + 7.5km Skiathlon', 'Cross-Country Skiing', '15:45:00', '2022-02-05'); -- 6
INSERT INTO Events(name, discipline, time, date) VALUES ('Mixed Doubles Round Robin Session 3', 'Curling', '14:05:00', '2022-02-03'); -- 7
INSERT INTO Events(name, discipline, time, date) VALUES ('Team Event - Men Single Skating - Short Program', 'Figure Skating',
														'09:55:00', '2022-02-04'); -- 7
INSERT INTO Events(name, discipline, time, date, location) VALUES ('Men''s Sprint Free Final', 'Cross-Country Skiing', '20:11:30', '2022-02-08', 
																	'Zhangjiakou National Cross-Country Skiing Centre'); -- 8
INSERT INTO Events(name, discipline, time, date, location) VALUES ('Men''s Aerials Final 1', 'Freestyle Skiing', '19:00:00', '2022-02-16',
																	'Genting Snow Park A & M Stadium'); -- 9
INSERT INTO Events(name, discipline, time, date, location) VALUES ('Women''s Snowboard Big Air Final Run 1', 'Snowboard', '09:30:00', '2022-02-15',
																	'Big Air Shougang'); -- 10

-- 2. Insert data into the Countries table
INSERT INTO Countries(name, code) VALUES ('Norway', 'NOR');
INSERT INTO Countries(name, code) VALUES ('Germany', 'GER');
INSERT INTO Countries(name, code) VALUES ('United States of America', 'USA');
INSERT INTO Countries(name, code) VALUES ('Sweden', 'SWE');
INSERT INTO Countries(name, code) VALUES ('Canada', 'CAN');
INSERT INTO Countries(name, code) VALUES ('Great Britain', 'GBR');
INSERT INTO Countries(name, code) VALUES ('France', 'FRA');
INSERT INTO Countries(name, code) VALUES ('Switzerland', 'SUI');
INSERT INTO Countries(name, code) VALUES ('Netherlands', 'NED');
INSERT INTO Countries(name, code) VALUES ('Austria', 'AUT');
INSERT INTO Countries(name, code) VALUES ('People''s Republic of China', 'CHN');
INSERT INTO Countries(name, code) VALUES ('ROC', 'ROC'); -- Russian Federation
INSERT INTO Countries(name, code) VALUES ('Japan', 'JPN');
INSERT INTO Countries(name, code) VALUES ('Italy', 'ITA');
INSERT INTO Countries(name, code) VALUES ('Republic of Korea', 'KOR');
INSERT INTO Countries(name, code) VALUES ('Slovenia', 'SLO');
INSERT INTO Countries(name, code) VALUES ('Finland', 'FIN');
INSERT INTO Countries(name, code) VALUES ('New Zealand', 'NZL');
INSERT INTO Countries(name, code) VALUES ('Australia', 'AUS');
INSERT INTO Countries(name, code) VALUES ('Hungary', 'HUN');
INSERT INTO Countries(name, code) VALUES ('Belgium', 'BEL');
INSERT INTO Countries(name, code) VALUES ('Czech Republic', 'CZE');
INSERT INTO Countries(name, code) VALUES ('Slovakia', 'SVK');
INSERT INTO Countries(name, code) VALUES ('Belarus', 'BLR');
INSERT INTO Countries(name, code) VALUES ('Spain', 'ESP');
INSERT INTO Countries(name, code) VALUES ('Ukraine', 'UKR');
INSERT INTO Countries(name, code) VALUES ('Estonia', 'EST');
INSERT INTO Countries(name, code) VALUES ('Latvia', 'LAT');
INSERT INTO Countries(name, code) VALUES ('Poland', 'POL');

-- 3. Insert data into the Athletes table
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 1
            VALUES ('AERNI Luca', 'Male', 'SUI', '1993-03-27', '185', 'SUI', 'SUI');
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 2
            VALUES ('ANDERSSON Ebba', 'Female', 'SWE', '1997-07-10', '155', 'SWE', 'SWE');
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 3
            VALUES ('BARBEZAT Melanie', 'Female', 'SUI', '1993-03-27', '176', 'SUI', 'SUI');
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 4
            VALUES ('BARBERIO Mark', 'Male', 'CAN', '1993-03-27', '185', 'ROC', 'CAN');
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 4
            VALUES ('BATES Evan', 'Male', 'USA', '1989-02-23', '185', 'CAN', 'USA');
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 6
            VALUES ('BAUER Florian', 'Male', 'GER', '1994-02-11', '185', 'GER', 'GER'); -- Bobsleigh
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 7
            VALUES ('BEAUVAIS Cesar', 'Male', 'BEL', '2000-08-17', '185', 'FRA', 'FRA'); -- Biathlon
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 8
            VALUES ('BELLINGHAM Phil', 'Male', 'AUS', '1991-02-24', '184', 'AUS', 'AUS'); -- Cross-Country Skiing
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 9
            VALUES ('BENTZ Coralie', 'Female', 'FRA', '1996-05-21', '163', 'FRA', 'FRA'); -- Cross-Country Skiing
INSERT INTO Athletes(name, gender, country_code, DOB, height_in_cm, residence_country_code, birth_country_code) -- 10
            VALUES ('ULBING Daniela', 'Female', 'AUT', '1998-02-27', '182', 'AUT', 'AUT'); -- Snowboard
            
-- 4. Insert data into the OlympicCommitteeMembers table
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('BEIGHTON Sean', 'Male', 'USA'); -- 1
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('CHARETTE Pierre', 'Female', 'SUI'); -- 2 
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('ROGERS Heather', 'Female', 'BEL'); -- 3
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('ENGDAHL Thomas', 'Male', 'SWE'); -- 4
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('BEIGHTON Sean', 'Male', 'USA'); -- 5
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('PETERS Laine', 'Female', 'USA'); -- 6
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('ISKIW Beth', 'Female', 'CAN'); -- 7
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('GJELSVIK Roger', 'Male', 'NOR'); -- 8
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('JULIEN Claude', 'Male', 'CAN'); -- 9
INSERT INTO OlympicCommitteeMembers(name, gender, country_code) VALUES ('LINDSTROM Kristian', 'Male', 'GBR'); -- 10

-- 5. Insert data into the Roles table
INSERT INTO Roles(member_ID, role_name) VALUES (1, 'Chairperson');
INSERT INTO Roles(member_ID, role_name) VALUES (1, 'Curling Event Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (1, 'Curling Venue Coordinator');
INSERT INTO Roles(member_ID, role_name) VALUES (1, 'Curling Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (2, 'Treasurer');
INSERT INTO Roles(member_ID, role_name) VALUES (2, 'Venue Coordinator');
INSERT INTO Roles(member_ID, role_name) VALUES (3, 'Cross-Country Skiing Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (3, 'Events Coordinator');
INSERT INTO Roles(member_ID, role_name) VALUES (3, 'Communication Lead');
INSERT INTO Roles(member_ID, role_name) VALUES (4, 'Programme Coordinator');
INSERT INTO Roles(member_ID, role_name) VALUES (5, 'Venue Coordinator');
INSERT INTO Roles(member_ID, role_name) VALUES (6, 'Communicaions Lead');
INSERT INTO Roles(member_ID, role_name) VALUES (6, 'Curling Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (6, 'Alpine Skiing  Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (6, 'Bobsleigh  Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (7, 'Alpine Skiing Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (7, 'Bobsleigh  Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (7, 'Freestyle Skiing Event Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (8, 'Cross-Country Skiing Event Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (8, 'Figure Skating Event Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (8, 'Biathlon Event Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (9, 'Figure Skating Event Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (9, 'Bobsleigh  Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (9, 'Freestyle Skiing  Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (9, 'Cross-Country Skiing  Medal Ceremony Organizer');
INSERT INTO Roles(member_ID, role_name) VALUES (10, 'Hospitality Coordinator');
INSERT INTO Roles(member_ID, role_name) VALUES (10, 'Biathlon  Medal Ceremony Organizer');

-- 6. Insert data into the AchievedResults table
-- athlete_ID 3 will not have any results since it is possible an athlete will not participate in any events
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (1, 5, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (1, 6, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (1, 4, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (2, 1, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (2, 5, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (2, 7, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (3, 7, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (3, 4, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (3, 2, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (4, 6, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (4, 7, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (4, 8, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (5, 10, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (5, 9, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (5, 6, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (6, 1, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (6, 9, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (6, 4, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (7, 8, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (7, 7, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (7, 6, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (8, 10, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (8, 9, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (8, 8, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (9, 1, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (9, 2, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (9, 5, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (10, 2, 1);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (10, 4, 2);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (10, 5, 3);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (1, 7, 6);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (1, 2, 8);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (1, 1, 7);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (2, 8, 4);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (2, 9, 7);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (2, 10, 10);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (3, 1, 7);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (3, 6, 9);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (3, 5, 8);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (4, 4, 5);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (4, 2, 9);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (4, 1, 6);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (5, 4, 10);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (5, 1, 15);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (5, 7, 13);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (6, 2, 12);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (6, 7, 22);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (6, 6, 6);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (7, 1, 8);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (7, 2, 10);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (7, 4, 13);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (8, 4, 11);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (8, 5, 13);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (8, 6, 22);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (9, 4, 7);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (9, 7, 6);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (9, 8, 9);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (10, 10, 11);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (10, 9, 7);
INSERT INTO AchievedResults(event_ID, athlete_ID, result) VALUES (10, 8, 8);


-- --- Views ----
-- DROP VIEW IF EXISTS <view_name>

-- Number of athletes from each country --
CREATE OR REPLACE VIEW num_athletes_by_country AS
SELECT country, COUNT(country_code)
FROM country_athlete_represents
GROUP BY country_code;

-- Number of participants in each event -- 
CREATE OR REPLACE VIEW participants_in_event AS
SELECT e.name, COUNT(e.name)
FROM Events AS e
INNER JOIN AchievedResults 
ON AchievedResults.event_ID = e.ID
GROUP BY e.ID;

-- Total of medals won by each country
CREATE OR REPLACE VIEW country_medal_count AS
SELECT c.name, COUNT(code) AS num_medals
FROM Countries AS c
INNER JOIN (SELECT ar.athlete_ID AS athlete_ID, a.country_code AS country_code, 
			ar.event_ID AS event_ID, ar.result AS position
			FROM AchievedResults AS ar
			INNER JOIN Athletes AS a
			WHERE (a.ID = ar.athlete_ID) AND (ar.result IN (1, 2, 3))) AS tmp
ON tmp.country_code = c.code
GROUP BY c.code
ORDER BY num_medals DESC;

-- Information about athlete, country and event
CREATE OR REPLACE VIEW athlete_event_participate AS
SELECT temp2.athlete_ID, temp2.athlete_name, temp2.country, e.name AS event, temp2.position
FROM Events AS e
INNER JOIN (SELECT temp1.athlete_ID, temp1.athlete_name, c.name AS country, temp1.event_ID, temp1.position
			FROM Countries AS c
			INNER JOIN (SELECT ar.athlete_ID AS athlete_ID, a.name AS athlete_name, 
						a.country_code AS country_code, ar.event_ID AS event_ID, ar.result AS position
						FROM AchievedResults AS ar
						INNER JOIN Athletes AS a
						WHERE a.ID = ar.athlete_ID) AS temp1
ON temp1.country_code = c.code) AS temp2
ON temp2.event_ID = e.ID;

-- Olympic committe roles and represent which country -- 
CREATE OR REPLACE VIEW organizing_committee AS
SELECT r.member_ID, ocm.name, r.role_name AS role, ocm.country_code
FROM Roles as r
INNER JOIN OlympicCommitteeMembers AS ocm
ON ocm.ID = r.member_ID;

-- no. of people responsible for each kind of event
CREATE OR REPLACE VIEW types_of_roles AS
SELECT r.role_name AS role, count(r.role_name)
FROM Roles as r
GROUP BY role_name;

-- Committee member information -- 
CREATE OR REPLACE VIEW committee_member_information AS
SELECT temp.member_ID, temp.name, c.name AS country, temp.role
FROM Countries as c
INNER JOIN (SELECT r.member_ID, ocm.name, r.role_name AS role, ocm.country_code
			FROM Roles as r
			INNER JOIN OlympicCommitteeMembers AS ocm
			ON ocm.ID = r.member_ID) AS temp
ON temp.country_code = c.code;

-- Number of committee members from each country --
CREATE OR REPLACE VIEW num_committee_members_by_country AS
SELECT c.name, COUNT(c.code) AS num_of_committee_members
FROM Countries AS c
INNER JOIN OlympicCommitteeMembers AS ocm
ON ocm.country_code = c.code
GROUP BY c.code
ORDER BY num_of_committee_members;

-- Country Gold Medals -- 
CREATE OR REPLACE VIEW country_gold_medals AS
SELECT c.code, c.name, COUNT(tmp.position) AS Gold
FROM Countries AS c
INNER JOIN (SELECT ar.athlete_ID AS athlete_ID, a.country_code AS country_code, 
			ar.event_ID AS event_ID, ar.result AS position
			FROM AchievedResults AS ar
			INNER JOIN Athletes AS a
			WHERE (a.ID = ar.athlete_ID) AND (ar.result = 1)) AS tmp
ON tmp.country_code = c.code
GROUP BY c.code
ORDER BY Gold DESC;

-- Country Silver Medals -- 
CREATE OR REPLACE VIEW country_silver_medals AS
SELECT c.code, c.name, COUNT(tmp.position) AS Silver
FROM Countries AS c
INNER JOIN (SELECT ar.athlete_ID AS athlete_ID, a.country_code AS country_code,
			ar.event_ID AS event_ID, ar.result AS position
			FROM AchievedResults AS ar
			INNER JOIN Athletes AS a
			WHERE (a.ID = ar.athlete_ID) AND (ar.result = 2)) AS tmp
ON tmp.country_code = c.code
GROUP BY c.code
ORDER BY Silver DESC;

-- Country Bronze Medals -- 
CREATE OR REPLACE VIEW country_bronze_medals AS
SELECT c.code, c.name, COUNT(tmp.position) AS Bronze
FROM Countries AS c
INNER JOIN (SELECT ar.athlete_ID AS athlete_ID, a.country_code AS country_code,
			ar.event_ID AS event_ID, ar.result AS position
			FROM AchievedResults AS ar
			INNER JOIN Athletes AS a
			WHERE (a.ID = ar.athlete_ID) AND (ar.result = 3)) AS tmp
ON tmp.country_code = c.code
GROUP BY c.code
ORDER BY Bronze DESC;

-- TMP TABLE FOR FULL JOIN --
CREATE OR REPLACE VIEW tmp_medal_tally AS
SELECT cgm.code AS country_code, cgm.name AS country, cgm.Gold, csm.Silver 
FROM country_gold_medals AS cgm
LEFT JOIN country_silver_medals AS csm
ON cgm.code = csm.code
UNION
SELECT csm.code AS country_code, csm.name AS country, cgm.Gold, csm.Silver 
FROM country_gold_medals AS cgm
RIGHT JOIN country_silver_medals AS csm
ON cgm.code = csm.code
ORDER BY Gold DESC, Silver DESC;

-- Final Country Medal Tally --
CREATE OR REPLACE VIEW country_medal_tally AS
SELECT cbm.name AS country, tmt.Gold, tmt.Silver, cbm.Bronze
FROM country_bronze_medals AS cbm
LEFT JOIN tmp_medal_tally AS tmt
ON cbm.code = tmt.country_code
UNION 
SELECT tmt.country, tmt.GOLD, tmt.Silver, cbm.Bronze
FROM country_bronze_medals AS cbm
RIGHT JOIN tmp_medal_tally AS tmt
ON cbm.code = tmt.country_code
ORDER BY Gold DESC, Silver DESC, Bronze DESC; -- order by gold first

-- Committee members responsibilities 
CREATE OR REPLACE VIEW num_events_organized AS
SELECT member_ID as committe_member_ID, name AS committee_member_name, COUNT(*) AS num_events
FROM committee_member_information
GROUP BY committe_member_id
ORDER BY num_events DESC;

-- GOLD MEDAL ATHLETES
CREATE OR REPLACE VIEW gold_medal_athletes AS
SELECT athlete_ID, athlete_name, COUNT(*) AS gold_medals
FROM athlete_event_participate -- has all positions as long they participated
WHERE position = 1
GROUP BY athlete_ID
ORDER BY gold_medals DESC;

-- SILVER MEDAL ATHLETES
CREATE OR REPLACE VIEW silver_medal_athletes AS
SELECT athlete_ID, athlete_name, COUNT(*) AS silver_medals
FROM athlete_event_participate -- has all positions as long they participated
WHERE position = 2
GROUP BY athlete_ID
ORDER BY silver_medals DESC;

-- BRONZE MEDAL ATHLETES
CREATE OR REPLACE VIEW bronze_medal_athletes AS
SELECT athlete_ID, athlete_name, COUNT(*) AS bronze_medals
FROM athlete_event_participate -- has all positions as long they participated
WHERE position = 3
GROUP BY athlete_ID
ORDER BY bronze_medals DESC;

-- TMP TABLE FOR FULL JOIN --
CREATE OR REPLACE VIEW tmp_athlete_medal_tally AS
SELECT gma.athlete_ID, gma.athlete_name, gma.gold_medals, sma.silver_medals 
FROM gold_medal_athletes AS gma
LEFT JOIN silver_medal_athletes AS sma
ON gma.athlete_ID = sma.athlete_ID
UNION
SELECT sma.athlete_ID, sma.athlete_name, gma.gold_medals, sma.silver_medals 
FROM gold_medal_athletes AS gma
RIGHT JOIN silver_medal_athletes AS sma
ON gma.athlete_ID = sma.athlete_ID
ORDER BY gold_medals DESC, silver_medals DESC;

-- Final Athlete Medal Tally --
CREATE OR REPLACE VIEW athlete_medal_tally AS
SELECT bma.athlete_ID, bma.athlete_name, tamt.gold_medals, tamt.silver_medals, bma.bronze_medals
FROM bronze_medal_athletes AS bma
LEFT JOIN tmp_athlete_medal_tally AS tamt
ON bma.athlete_ID = tamt.athlete_ID
UNION 
SELECT tamt.athlete_ID, tamt.athlete_name, tamt.gold_medals, tamt.silver_medals, bma.bronze_medals
FROM bronze_medal_athletes AS bma
RIGHT JOIN tmp_athlete_medal_tally AS tamt
ON bma.athlete_ID = tamt.athlete_ID
ORDER BY gold_medals DESC, silver_medals DESC, bronze_medals DESC; -- order by gold first

-- Total gold, silver, bronze, medals for each athlete
CREATE OR REPLACE VIEW total_athlete_medals AS
SELECT *, COALESCE(gold_medals, 0)  + COALESCE(silver_medals, 0) + COALESCE(bronze_medals, 0) AS total
FROM athlete_medal_tally
ORDER BY gold_medals DESC, silver_medals DESC, bronze_medals DESC, total DESC; -- order by gold first

-- SELECT QUERIES --
-- 1. Country with maximum medals 
CREATE OR REPLACE VIEW total_country_medals AS
SELECT *, COALESCE(Gold, 0)  + COALESCE(Silver, 0) + COALESCE(Bronze, 0) AS total
FROM country_medal_tally
ORDER BY total DESC;

-- by medals
SELECT *
FROM total_country_medals
WHERE total = (SELECT MAX(total) FROM total_country_medals);

-- by name
SELECT *
FROM total_country_medals
WHERE total = (SELECT MAX(total) FROM total_country_medals)
ORDER BY country;

-- 2. Athlete with max event participation
CREATE OR REPLACE VIEW num_events_participated_in AS
SELECT athlete_name, COUNT(*) AS num_events
FROM athlete_event_participate -- has all positions as long they participated
GROUP BY athlete_name
ORDER BY num_events DESC;

-- by name
SELECT *
FROM num_events_participated_in
WHERE num_events = (SELECT MAX(num_events) FROM num_events_participated_in)
ORDER BY athlete_name;

-- 3. Athlete with max gold/silver/bronze medals

-- gold
SELECT *
FROM gold_medal_athletes
WHERE gold_medals = (SELECT MAX(gold_medals) FROM gold_medal_athletes)
ORDER BY athlete_name;

-- silver
SELECT *
FROM silver_medal_athletes
WHERE silver_medals = (SELECT MAX(silver_medals) FROM silver_medal_athletes)
ORDER BY athlete_name;

-- bronze
SELECT *
FROM bronze_medal_athletes
WHERE bronze_medals = (SELECT MAX(bronze_medals) FROM bronze_medal_athletes)
ORDER BY athlete_name;

-- 3. Athlete with maximum medals

-- by points
SELECT *
FROM total_athlete_medals
WHERE total = (SELECT MAX(total) FROM total_athlete_medals);

-- by name
SELECT *
FROM total_athlete_medals
WHERE total = (SELECT MAX(total) FROM total_athlete_medals)
ORDER BY athlete_name;

-- 4. Committee member with most responsibility --
SELECT *
FROM num_events_organized
WHERE num_events = (SELECT MAX(num_events) FROM num_events_organized);

-- 5. Athletes that do not participate in any events --
SELECT ID, name
FROM Athletes AS a
WHERE a.ID NOT IN (select athlete_id
                 from AchievedResults);
                 
-- 6. Who is giving medals for the Alpine Skiing Event --
SELECT ocm.ID, ocm.name
FROM Roles AS r
INNER JOIN OlympicCommitteeMembers AS ocm
ON r.member_ID = ocm.ID
WHERE r.role_name = 'Alpine Skiing Medal Ceremony Organizer';

-- country representation in athletes and committees --

-- 7. No. of Male and female Athletes --
SELECT COUNT(*) AS Female_participants
FROM Athletes
WHERE gender = 'Female';

SELECT COUNT(*) AS Male_participants
FROM Athletes
WHERE gender = 'Male';

-- 8. No. of Male and female Committee Members --
SELECT COUNT(*) 
FROM OlympicCommitteeMembers
WHERE gender = 'Male';

SELECT COUNT(*) 
FROM OlympicCommitteeMembers
WHERE gender = 'Female';

-- TODO: ADDITIONAL FEATURES (OPTIONAL)

-- TRIGGERS --

-- LOCK TABLES t1 WRITE; -- the next prompt appears once you've obtained the lock
-- DROP TRIGGER IF EXISTS invalid_role2;
-- DROP TRIGGER t1_bi; 

-- 1. INVALID ROLE (update and insert)
delimiter //
CREATE TRIGGER invalid_role_insert BEFORE INSERT
ON Roles
FOR EACH ROW
IF NEW.member_ID < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid member ID';
END IF; //
delimiter ;

delimiter //
CREATE TRIGGER invalid_role_update BEFORE UPDATE
ON Roles
FOR EACH ROW
IF NEW.member_ID < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid member ID';
END IF; //
delimiter ;

-- 2. INVALID EVENT (update and insert)
delimiter //
CREATE TRIGGER invalid_event_insert BEFORE INSERT
ON AchievedResults
FOR EACH ROW
IF NEW.event_ID < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid event ID';
END IF; //
delimiter ;

delimiter //
CREATE TRIGGER invalid_event_update BEFORE UPDATE
ON AchievedResults
FOR EACH ROW
IF NEW.event_ID < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid event ID';
END IF; //
delimiter ;

-- 3. INVALID ATHLETE (insert and update)
delimiter //
CREATE TRIGGER invalid_athlete_insert BEFORE INSERT
ON AchievedResults
FOR EACH ROW
IF NEW.athlete_ID < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid athlete ID';
END IF; //
delimiter ;

delimiter //
CREATE TRIGGER invalid_athlete_update BEFORE UPDATE
ON AchievedResults
FOR EACH ROW
IF NEW.athlete_ID < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid athlete ID';
END IF; //
delimiter ;

-- 4. INVALID RESULT (insert and update)
delimiter //
CREATE TRIGGER invalid_result_insert BEFORE INSERT
ON AchievedResults
FOR EACH ROW
IF NEW.result < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid result';
END IF; //
delimiter ;

delimiter //
CREATE TRIGGER invalid_result_update BEFORE UPDATE
ON AchievedResults
FOR EACH ROW
IF NEW.result < 1 THEN
SIGNAL SQLSTATE '50001' SET MESSAGE_TEXT = 'Invalid result';
END IF; //
delimiter ;

-- todo: create roles
-- todo: create users (account level, relational level)

-- User Priviliges -- 
-- 1. Database Administrator (DBA) 
-- 2. Developers (server-side)
-- 3. Application (cient-side)

-- CREATE USER dba_user IDENTIFIED BY xy*ttax467
-- GRANT ALL PRIVILEGES TO dba_user;
-- GRANT CONNECT, RESOURCE, DBA TO dba_user;
-- GRANT CREATE SESSION, GRANT ANY PRIVILEGE TO dba_user
-- GRANT UNLIMITED TABLESPACE TO dba_user
-- GRANT CREATE TABLE to dba_user

-- CREATE USER developer_server_side IDENTIFIED BY zyt&2003c
-- GRANT CONNECT TO developer_server_side
-- GRANT CREATE SESSION TO app_client_side
-- GRANT UNLIMITED TABLESPACE TO developer_server_side
-- GRANT SELECT, INSERT, DELETE, ALTER, UPDATE ON Athletes,Countries,Events,Roles,OlympicCommitteeMembers,AchievedResults to developer_server_side WITH GRANT OPTION;

-- CREATE USER app_client_side IDENTIFIED BY grt$pie
-- GRANT CONNECT TO app_client_side
-- GRANT CREATE SESSION TO app_client_side
-- GRANT SELECT ON Athletes,Countries,Events,Roles,OlympicCommitteeMembers,AchievedResults TO app_client_side;

-- ------------------------- SYNTAX -------------------------------
-- GRANT privilege,[privilege],.. 
-- ON privilege_level 
-- TO user [IDENTIFIED BY password] 
-- [REQUIRE tsl_option] 
-- [WITH [GRANT_OPTION | resource_option]];

-- GRANT ALL PRIVILEGES ON $2.* TO \"$1\"@\"localhost\" IDENTIFIED BY \"$password\";

-- CREATE USER 'user_name'@'localhost' IDENTIFIED BY 'password';
-- mysql> GRANT ALL PRIVILEGES ON database_name.* TO 'user_name'@'localhost' WITH GRANT OPTION;
-- mysql> CREATE USER 'user_name'@'%' IDENTIFIED BY 'password';
-- mysql> GRANT ALL PRIVILEGES ON database_name.* TO 'user_name'@'%' WITH GRANT OPTION;

-- CREATE SCHEMA
-- CREATE TABLE
-- CREATE VIEW
-- ALTER
-- DROP 

-- TODO: ADD ROLES

-- TODO: IMPLEMENT ROLE SPECIFICATIONS FOR ALL (3)
-- GRANT create table, create view
-- TO manager;

-- FOR ADMIN, DBA
-- GRANT ALL PRIVILEGES, CONNECT, RESOURCE, CREATE SESSION, ANY PRIVILEGE, 
-- UNLIMITED TABLESPACE, CREATE TABLE
-- TO DBA

-- USERS -- 
CREATE USER yuvika;
CREATE USER user_client;
CREATE USER user_server;

-- ROLES --
CREATE ROLE dev;
CREATE ROLE app;
CREATE ROLE admin_dba;

GRANT dev TO user_server;
GRANT app TO user_client;
GRANT admin_dba TO yuvika;

-- admin_dba access -- 
GRANT DBA
TO admin_dba;

-- Cannot give access to multiple tables in one line and we need to either grant them individually or use
-- a script in PL/SQL to do so. 

-- GRANT CREATE VIEW, SELECT
-- ON Athletes,Countries,Events,Roles,OlympicCommitteeMembers,AchievedResults
-- TO app;

-- GRANT CREATE VIEW, SELECT, INSERT, DELETE, UPDATE 
-- ON Athletes,Countries,Events,Roles,OlympicCommitteeMembers,AchievedResults
-- TO dev WITH GRANT OPTION;

-- user_client access -- 
GRANT CREATE VIEW
TO app;

GRANT SELECT
ON Athletes
TO app;
GRANT SELECT
ON Countries
TO app;
GRANT SELECT
ON Events
TO app;
GRANT SELECT
ON Roles
TO app;
GRANT SELECT
ON OlympicCommitteeMembers
TO app;
GRANT SELECT
ON AchievedResults
TO app;

-- user_server access -- 
GRANT CREATE VIEW
TO dev;

GRANT SELECT
ON Athletes
TO dev WITH GRANT OPTION;
GRANT SELECT
ON Countries
TO dev WITH GRANT OPTION;
GRANT SELECT
ON Events
TO dev WITH GRANT OPTION;
GRANT SELECT
ON Roles
TO dev WITH GRANT OPTION;
GRANT SELECT
ON OlympicCommitteeMembers
TO dev WITH GRANT OPTION;
GRANT SELECT
ON AchievedResults
TO dev WITH GRANT OPTION;

GRANT INSERT
ON Athletes
TO dev WITH GRANT OPTION;
GRANT INSERT
ON Countries
TO dev WITH GRANT OPTION;
GRANT INSERT
ON Events
TO dev WITH GRANT OPTION;
GRANT INSERT
ON Roles
TO dev WITH GRANT OPTION;
GRANT INSERT
ON OlympicCommitteeMembers
TO dev WITH GRANT OPTION;
GRANT INSERT
ON AchievedResults
TO dev WITH GRANT OPTION;

GRANT DELETE
ON Athletes
TO dev WITH GRANT OPTION;
GRANT DELETE
ON Countries
TO dev WITH GRANT OPTION;
GRANT DELETE
ON Events
TO dev WITH GRANT OPTION;
GRANT DELETE
ON Roles
TO dev WITH GRANT OPTION;
GRANT DELETE
ON OlympicCommitteeMembers
TO dev WITH GRANT OPTION;
GRANT DELETE
ON AchievedResults
TO dev WITH GRANT OPTION;

GRANT UPDATE
ON Athletes
TO dev WITH GRANT OPTION;
GRANT UPDATE
ON Countries
TO dev WITH GRANT OPTION;
GRANT UPDATE
ON Events
TO dev WITH GRANT OPTION;
GRANT UPDATE
ON Roles
TO dev WITH GRANT OPTION;
GRANT UPDATE
ON OlympicCommitteeMembers
TO dev WITH GRANT OPTION;
GRANT UPDATE
ON AchievedResults
TO dev WITH GRANT OPTION;

-- the above is time consuming and there's more room for error, hence we can write the run script


-- PL/SQL script to GRANT SELECT/INSERT/UPDATE/DELETE access to multiple tables
-- for SELECT --
CREATE PROCEDURE grant_select(
    username VARCHAR(10), 
    grantee VARCHAR(10)
AS   
BEGIN
    FOR r IN (
        SELECT owner, table_name 
        FROM all_tables 
        WHERE owner = username
    )
    LOOP
        EXECUTE IMMEDIATE 
            'GRANT SELECT ON '||r.owner||'.'||r.table_name||' to ' || grantee;
    END LOOP;
END;

-- for INSERT --
CREATE PROCEDURE grant_insert(
    username VARCHAR(10), 
    grantee VARCHAR(10)
AS   
BEGIN
    FOR r IN (
        SELECT owner, table_name 
        FROM all_tables 
        WHERE owner = username
    )
    LOOP
        EXECUTE IMMEDIATE 
            'GRANT INSERT ON '||r.owner||'.'||r.table_name||' to ' || grantee;
    END LOOP;
END;

-- for DELETE
CREATE PROCEDURE grant_delete(
    username VARCHAR(10), 
    grantee VARCHAR(10)
AS   
BEGIN
    FOR r IN (
        SELECT owner, table_name 
        FROM all_tables 
        WHERE owner = username
    )
    LOOP
        EXECUTE IMMEDIATE 
            'GRANT DELETE ON '||r.owner||'.'||r.table_name||' to ' || grantee;
    END LOOP;
END;

-- for UPDATE --
CREATE PROCEDURE grant_update(
    username VARCHAR(10), 
    grantee VARCHAR(10)
AS   
BEGIN
    FOR r IN (
        SELECT owner, table_name 
        FROM all_tables 
        WHERE owner = username
    )
    LOOP
        EXECUTE IMMEDIATE 
            'GRANT UPDATE ON '||r.owner||'.'||r.table_name||' to ' || grantee;
    END LOOP;
END;

EXEC grant_select('yuvika','app');
EXEC grant_select('yuvika','dev');
EXEC grant_insert('yuvika','dev');
EXEC grant_delete('yuvika','dev');
EXEC grant_update('yuvika','dev');

-- Backup database

-- # Script to back up database every day at 4:00 AM
-- #!/bin/sh
-- # mysql_backup.sh

-- # Use this utility to schedule back up at 4:00 AM

-- # sudo apt-get update 
-- # sudo apt-get install postfix mailutils
-- # sudo crontab -e
-- # 00 04 * * * /home/user/script/mysql_backup.sh

-- # Backup storage directory 
-- backupfolder=/var/backups

-- # Notification email address 
-- recipient_email=<khardeny@tcd.ie>
-- # MySQL user
-- user=yuvika
-- # MySQL password
-- password=trinityCourses2022
-- # Number of days to store the backup 
-- keep_day=30 

-- sqlfile=$backupfolder/all-database-$(date +%d-%m-%Y_%H-%M-%S).sql
-- zipfile=$backupfolder/all-database-$(date +%d-%m-%Y_%H-%M-%S).zip 

-- # Create a backup 
-- sudo mysqldump -u $user -p$password --all-databases > $sqlfile 
-- if [ $? == 0 ]; then
--   echo 'Sql dump created' 
-- else
--   echo 'mysqldump return non-zero code' | mailx -s 'No backup was created!' $recipient_email  
--   exit 
-- fi 

-- # Compress backup 
-- zip $zipfile $sqlfile 
-- if [ $? == 0 ]; then
--   echo 'The backup was successfully compressed' 
-- else
--   echo 'Error compressing backup' | mailx -s 'Backup was not created!' $recipient_email 
--   exit 
-- fi

-- rm $sqlfile 
-- echo $zipfile | mailx -s 'Backup was successfully created' $recipient_email 

-- # Delete old backups 
-- find $backupfolder -mtime +$keep_day -delete
